## Terminologie MII KDS Basismodul Person

Dieses Paket enthält die Terminologie aus dem Basismodul Person des Kerndatensatzes der MII.

Diese Ressourcen wurden umpaketiert aus dem Ursprungspaket: https://simplifier.net/packages/de.medizininformatikinitiative.kerndatensatz.person/2025.0.0.

---

Sobald neue Versionen veröffentlicht werden, werden diese auf anderen Git-Branches verfügbar gemacht. Sie sehen aktuell den Stand vom Branch **2025.0.0**.

Weitere Branches:
- [2024.0.0](https://gitlab.com/mii-termserv/fhir-resources/mii-kerndatensatz/de.medizininformatikinitiative.kerndatensatz.terminology.person/-/tree/2024.0.0?ref_type=heads)
